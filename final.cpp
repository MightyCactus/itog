#include <SFML/Graphics.hpp>
#include <iostream>
#include <windows.h>
#include <vector>
#include <cstdlib>

class rain : public sf::RectangleShape
{
public:
	float speed;
	rain(float x, float y, sf::RenderWindow& w) : sf::RectangleShape(sf::Vector2f(x, y))
	{
		setFillColor(sf::Color(100, 100, 255));
		setPosition(rand() % w.getSize().x - 1, -20);
		speed = rand() % 10 + 4;
	};

	void returnToUp(sf::RenderWindow& w)
	{
		setPosition(rand() % w.getSize().x - 1, -20);
		speed = rand() % 10 + 4;
	}
};

int main()
{

	int n;

	std::cout << "Vvedite chislo kapel: ";
	std::cin >> n;

	while (n <= 0)
	{
		std::cout << "Chislo kapel doljno bit polojitelnim! Vvedite chislo kapel eshe raz: ";
		std::cin >> n;
	}

	std::vector <rain> RainVector;
	sf::RenderWindow window(sf::VideoMode(960, 540), "Rain in SMFL");
	window.setVerticalSyncEnabled(true);

	sf::Event e;
	while (window.isOpen())
	{
		while (window.pollEvent(e))
		{
			if (e.type == sf::Event::Closed || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) window.close();
		}

		for (int i = 0; i < RainVector.size(); i++)
		{
			float y;
			float v;
			y = RainVector[i].getPosition().y;
			v = RainVector[i].speed;
			if (y > window.getSize().y)
			{
				RainVector[i].returnToUp(window);
			}
			else
			{
				RainVector[i].speed += 0.1;
				RainVector[i].move(0, v);
			}
		}

		for (int i = 0; i < 4; i++)
		{
			if (RainVector.size() < n)
			{
				RainVector.push_back(rain(2, 20, window));
			}
		}

		//Yesli izmenit razmer okna, formi ne budut deformirovatsya.
		if (e.type == sf::Event::Resized) {
			float w = static_cast<float>(e.size.width);
			float h = static_cast<float>(e.size.height);
			window.setView(
				sf::View(
					sf::Vector2f(w / 2.0, h / 2.0),
					sf::Vector2f(w, h)
				)
			);
		}

		window.clear(sf::Color::Black);

		for (auto& drop : RainVector)
		{
			window.draw(drop);
		}

		window.display();
	}

	return 0;

}